package com.zuitt;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		String Fname = req.getParameter("fname");
		System.getProperties().put("fname", Fname);
		
		String Lname = req.getParameter("lname");
		HttpSession session = req.getSession();
		session.setAttribute("lname",Lname);
		
		ServletContext srvContext = getServletContext();
		String Email = req.getParameter("email");
		srvContext.setAttribute("email",Email);
				
		RequestDispatcher rd = req.getRequestDispatcher("detail");
		rd.forward(req, res);
	}
	
}
