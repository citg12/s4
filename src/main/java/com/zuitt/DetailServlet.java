package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1943764552249535889L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletContext srvContext = getServletContext();
		String brandname = srvContext.getInitParameter("brand_name");
		
		String Fname = System.getProperty("fname");
		
		HttpSession session = req.getSession();
		String Lname = session.getAttribute("lname").toString();
		
		String Email = (String)srvContext.getAttribute("email");
		
		String contact = req.getParameter("contact");
		
		
		
		
		PrintWriter output = res.getWriter();
		output.println(
			"<h1>"+brandname+"</h1>" +
			"<p>First Name: " + Fname + "</p>" +
			"<p>Last Name: " + Lname + "</p>" +
			"<p>Contact: " + contact + "</p>" +
			"<p>Email: " + Email + "</p>"
			);

	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		String contact = req.getParameter("contact");
		res.sendRedirect("detail?contact="+ contact);
		
	}
}
